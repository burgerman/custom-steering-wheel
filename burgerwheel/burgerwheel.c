#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <inttypes.h>

#define DEVNAME "/dev/hidg0"
#define BUF_LEN 512

void FfbOnUsbData(uint8_t* data, uint16_t len);

int main(int argc, const char *argv[]) {
    int fd;
    char buf[BUF_LEN];

    if ((fd = open(DEVNAME, O_RDWR, 0666)) == -1) {
        perror(DEVNAME);
        return 3;
    }
    fd_set rfds;

    while(1) {
        FD_ZERO(&rfds);
        FD_SET(STDIN_FILENO, &rfds);
        FD_SET(fd, &rfds);

        int retval = select(fd + 1, &rfds, NULL, NULL, NULL);
        if (retval == -1 && errno == EINTR)
            continue;
        if (retval < 0) {
            perror("select()");
            return 4;
        }

        if (FD_ISSET(fd, &rfds)) {
            size_t cmd_len = read(fd, buf, BUF_LEN - 1);
            printf("recv report:");
            for (int i = 0; i < cmd_len; i++)
                printf(" %02x", buf[i]);
            printf("\n");
            FfbOnUsbData((uint8_t*)buf, cmd_len);
        }
    }
}

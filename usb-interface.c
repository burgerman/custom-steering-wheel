#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <linux/spi/spidev.h>
#include <sys/ioctl.h>
#include <string.h>  // memset()

#include "ffb-lib/ffb.h"

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0') 

#define STEER_SENSITIVITY 4

typedef struct __attribute__((__packed__))
{
    uint8_t report_id;
    uint16_t wheel_pos;
    uint16_t pedal1;
    uint16_t pedal2;
    uint16_t pedal3;
    uint16_t hat_and_buttons; // first 4 bits are hat, last 12 bits are buttons
} USB_Report_Inputs_t;

// SPI settings
static uint8_t spi_mode = SPI_MODE_0;
static uint8_t spi_bits = 8;
static uint32_t spi_speed = 7500; // 7.5 kHz -> ~ 300 captures/sec (24 bits)

// SPI1 (MCP3008) settings
static uint8_t spi1_mode = SPI_MODE_0;
static uint8_t spi1_bits = 8;
static uint32_t spi1_speed = 1350000; // 1.35Mhz as per datasheet


int main (void)
{
    USB_Report_Inputs_t report = {1, 32767, 0, 0, 0, 0}, 
			     prev_report = {1, 0, 0, 0, 0, 0};

    long quad_dec_value = 0;

    int quad_dev_fd = 0; // File descriptor for quadrature driver (sysfs)
    int spi_dev0_fd = 0; // File descriptor for SPI talking to Thrustmaster wheel buttons
    int spi_dev1_fd = 0; // File descriptor for SPI talking to Thrustmaster pedals via MCP3008 ADC 
    FILE* hid_dev = 0; // FILE pointer for talking USB to host computer (USB gadget)

    hid_dev = fopen("/dev/hidg0", "r+");
    if(!hid_dev)
    {
        printf("Error opening /dev/hidg0\n");
        return 0;
    }

    // the quad decoder driver gives us a long value
    // starting a 0 and going +/- 4000 for each full rotation
    quad_dev_fd = open("/sys/kernel/quad_dec_count/enc_count", O_RDONLY);
    if(!quad_dev_fd)
    {
        printf("Error opening /sys/kernel/quad_dec_count/enc_count\n");
        return 0;
    }

    // the Thrusmaster wheel we are using sends button status over SPI
    // read-only required
    spi_dev0_fd = open("/dev/spidev0.0", O_RDONLY);
    if(!spi_dev0_fd)
    {
        printf("Error opening /dev/spidev0.0\n");
        return 0;
    }
    // Configuration of SPI link, read only
    ioctl(spi_dev0_fd, SPI_IOC_WR_MODE, &spi_mode);
    ioctl(spi_dev0_fd, SPI_IOC_WR_BITS_PER_WORD, &spi_bits);
    ioctl(spi_dev0_fd, SPI_IOC_WR_MAX_SPEED_HZ, &spi_speed);

    // pedals via MCP3008 ADC
    spi_dev1_fd = open("/dev/spidev0.1", O_RDWR);
    if(!spi_dev1_fd)
    {
        printf("Error opening /dev/spidev0.1\n");
        return 0;
    } 

    ioctl(spi_dev1_fd, SPI_IOC_WR_MODE, &spi1_mode);
    ioctl(spi_dev0_fd, SPI_IOC_WR_BITS_PER_WORD, &spi1_bits);
    ioctl(spi_dev0_fd, SPI_IOC_WR_MAX_SPEED_HZ, &spi1_speed);

    for(;;)
    {
        char buffer[16] = {0};

        // for a maximum of 1.5 rotations in each direction
        // at 4000 per full rotation
        // we get -6000 to +6000 that needs to be converted to
        // 0 to 65535
        // read long signed int from our quad driver
        // our USB report
        pread(quad_dev_fd, buffer, 16, 0);
        sscanf(buffer, "%ld\n", &quad_dec_value);
        // inverted GPIO wires on RPi, so let's just - for now
        quad_dec_value = -(quad_dec_value) * STEER_SENSITIVITY;
        // convert and set value in our USB report for transmission
        if(quad_dec_value > -6000 && quad_dec_value < 6000)
            report.wheel_pos = (quad_dec_value + 6000) * 5.46125;

        // read 3 bytes from wheel (Thrustmaster 599XX EVO 30)
        // 0 is press, 1 is released
        // we have 00101111 11111111 11111111
        // 0   -> 0
        // 1   -> 0
        // 2   -> 1
        // 3   -> PADDLE left
        // 4   -> PADDLE right
        // 5   -> PIT
        // 6   -> WASH
        // 7   -> RADIO
        //
        // 0   -> black button
        // 1   -> MAIN (CW)
        // 2   -> MAIN (CCW)
        // 3   -> SCROLL
        // 4   -> FLASH
        // 5   -> 1
        // 6   -> 1
        // 7   -> 1
        //
        // 0   -> D-PAD down
        // 1   -> D-PAD right
        // 2   -> D-PAD left
        // 3   -> D-PAD up
        // 4   -> 1
        // 5   -> 1
        // 6   -> 1
        // 7   -> 1
        
        memset(&buffer[0], 0, sizeof(buffer));
        read(spi_dev0_fd, buffer, 3);
        // first let's clear the bits in the USB report
        report.hat_and_buttons = 0;
        
        // hat has 8 positions - we send 0 to 7 equivalent to 0 to 315 degrees
        // let's retrieve the 4 bits of the 4 hat buttons
        uint8_t hat_value = ~(buffer[2] >> 4) & 15U;

        // up -> 0 degrees -> 0
        if(hat_value == 1) report.hat_and_buttons |= (uint8_t)0;
        // up/left -> 315 degrees -> 7
        else if(hat_value == 3) report.hat_and_buttons |= (uint8_t)7;
        // left -> 270 degrees -> 6
        else if(hat_value == 2) report.hat_and_buttons |= (uint8_t)6;
        // left/down -> 225 degrees -> 5
        else if(hat_value == 10) report.hat_and_buttons |= (uint8_t)5;
        // down -> 180 degrees -> 4
        else if(hat_value == 8) report.hat_and_buttons |= (uint8_t)4;
        // down/right -> 135 degrees -> 3
        else if(hat_value == 12) report.hat_and_buttons |= (uint8_t)3;
        // right -> 90  degrees -> 2
        else if(hat_value == 4) report.hat_and_buttons |= (uint8_t)2;
        // right/up -> 45 degrees  -> 1
        else if(hat_value == 5) report.hat_and_buttons |= (uint8_t)1;
        // nothing or bad combination set to out of range number
        else
            report.hat_and_buttons |= (uint8_t)10;

        // set bits for the 12 buttons
        report.hat_and_buttons |= !((buffer[0] >> 4) & 1U) << 4;
        report.hat_and_buttons |= !((buffer[0] >> 3) & 1U) << 5;
        report.hat_and_buttons |= !((buffer[0] >> 2) & 1U) << 6;
        report.hat_and_buttons |= !((buffer[0] >> 1) & 1U) << 7;
        report.hat_and_buttons |= !((buffer[0] >> 0) & 1U) << 8;
        report.hat_and_buttons |= !((buffer[1] >> 7) & 1U) << 9;
        report.hat_and_buttons |= !((buffer[1] >> 6) & 1U) << 10;
        report.hat_and_buttons |= !((buffer[1] >> 5) & 1U) << 11;
        report.hat_and_buttons |= !((buffer[1] >> 4) & 1U) << 12;
        report.hat_and_buttons |= !((buffer[1] >> 3) & 1U) << 13;

        //printf(""BYTE_TO_BINARY_PATTERN" "BYTE_TO_BINARY_PATTERN" "BYTE_TO_BINARY_PATTERN"\n",
        //        BYTE_TO_BINARY(report.hat_and_buttons>>16),BYTE_TO_BINARY(report.hat_and_buttons>>8), BYTE_TO_BINARY(report.hat_and_buttons));

        // Let's read our pedals
        uint16_t value[3];
        for(int chan = 0; chan < 3; chan++)
        {
            uint8_t tx[3] = {0x01, (0x08|chan)<<4, 0x00};
            uint8_t rx[3];
            struct spi_ioc_transfer tr = {
                .tx_buf = (unsigned long)tx,
                .rx_buf = (unsigned long)rx,
                .len = 3,
                .delay_usecs = 0,
                .speed_hz = spi1_speed,
                .bits_per_word = spi1_bits,
            };

            if(ioctl(spi_dev1_fd, SPI_IOC_MESSAGE(1), &tr) < 1)
                printf("can't send spi message to ADC");
            /*
            printf("sent """BYTE_TO_BINARY_PATTERN" "BYTE_TO_BINARY_PATTERN" "BYTE_TO_BINARY_PATTERN"\n",
                    BYTE_TO_BINARY(tx[0]), BYTE_TO_BINARY(tx[1]), BYTE_TO_BINARY(tx[2]));

            printf("got "BYTE_TO_BINARY_PATTERN" "BYTE_TO_BINARY_PATTERN" "BYTE_TO_BINARY_PATTERN"\n",
                    BYTE_TO_BINARY(rx[0]), BYTE_TO_BINARY(rx[1]), BYTE_TO_BINARY(rx[2]));
                    */

            value[chan] = ((int)rx[1] & 0x03) << 8 | (int)rx[2];
        }
        // min and max values (voltage level) of each pedal
        // let's start close to the middle and then auto-calibrate
        static uint16_t pedal1_limits[2] = {400, 500},
                 pedal2_limits[2] = {400, 500},
                 pedal3_limits[2] = {400, 500};

        // Let's auto-calibrate (reset boundaries according to actual value)
        if(value[0] < pedal1_limits[0]) pedal1_limits[0] = value[0];
        if(value[0] > pedal1_limits[1]) pedal1_limits[1] = value[0];
        
        if(value[1] < pedal2_limits[0]) pedal2_limits[0] = value[1];
        if(value[1] > pedal2_limits[1]) pedal2_limits[1] = value[1];

        if(value[2] < pedal3_limits[0]) pedal3_limits[0] = value[2];
        if(value[2] > pedal3_limits[1]) pedal3_limits[1] = value[2];

        // Let's set value to 0 when close to 0
#define LOW_SIDE_TOLERANCE 50
        if(value[0] < pedal1_limits[0] + LOW_SIDE_TOLERANCE) value[0] = pedal1_limits[0]; 
        if(value[1] < pedal2_limits[0] + LOW_SIDE_TOLERANCE) value[1] = pedal2_limits[0];
        if(value[2] < pedal3_limits[0] + LOW_SIDE_TOLERANCE) value[2] = pedal3_limits[0]; 
        report.pedal1 = (value[0] - pedal1_limits[0]) * (65535/(pedal1_limits[1] - pedal1_limits[0])); 
        report.pedal2 = (value[1] - pedal2_limits[0]) * (65535/(pedal2_limits[1] - pedal2_limits[0])); 
        report.pedal3 = (value[2] - pedal3_limits[0]) * (65535/(pedal3_limits[1] - pedal3_limits[0])); 

        /*
        printf("%d < %d < %d - %d < %d <%d - %d < %d < %d\n", pedal1_limits[0], value[0], pedal1_limits[1], pedal2_limits[0], value[1], pedal2_limits[1], pedal3_limits[0], value[2], pedal3_limits[1]);
        printf("%d %d %d\n", report.pedal1, report.pedal2, report.pedal3);
        */

        // send our report over the USB link only if there is change
	// TODO check USB/HID spec for parameters around persistence of reports
	if(report.wheel_pos != prev_report.wheel_pos
		|| report.pedal1 != prev_report.pedal1
		|| report.pedal2 != prev_report.pedal2
		|| report.pedal3 != prev_report.pedal3
		|| report.hat_and_buttons != prev_report.hat_and_buttons)
	{
		fwrite(&report, sizeof(USB_Report_Inputs_t), 1, hid_dev);
		fflush(hid_dev);
		prev_report = report;
	}
        //printf ("%ld %u\n", quad_dec_value, report.wheel_pos);
	
	uint8_t recv_buf;
	void * report = NULL;
	fflush(hid_dev);
	while(fread(&recv_buf, 1, 1, hid_dev))
	{
		printf("read report_id %d   ", recv_buf);
		printf("got "BYTE_TO_BINARY_PATTERN"\n", BYTE_TO_BINARY(recv_buf));
		// let's check the report ID
		switch(recv_buf)
		{
			case RID_O_SET_EFFECT:
				report = malloc(sizeof(USB_FFBReport_SetEffect_Output_Data_t));
				fread(report, sizeof(USB_FFBReport_SetEffect_Output_Data_t), 1, hid_dev);
				printf("Set Effect Output Report read, sizeof %ld\n",
						sizeof(USB_FFBReport_SetEffect_Output_Data_t));
				printf("blidx %u, effect type %u duration %u, trigrepeatinterval %u,"
						"sample_period %u, gain %u, trigger_button %u," 
						"enableAxis %u, dX %u, dY %u\n",
						((USB_FFBReport_SetEffect_Output_Data_t *)report)->effectBlockIndex,
						((USB_FFBReport_SetEffect_Output_Data_t *)report)->effectType,
						((USB_FFBReport_SetEffect_Output_Data_t *)report)->duration,
						((USB_FFBReport_SetEffect_Output_Data_t *)report)->triggerRepeatInterval,
						((USB_FFBReport_SetEffect_Output_Data_t *)report)->samplePeriod,
						((USB_FFBReport_SetEffect_Output_Data_t *)report)->gain,
						((USB_FFBReport_SetEffect_Output_Data_t *)report)->triggerButton,
						((USB_FFBReport_SetEffect_Output_Data_t *)report)->enableAxis,
						((USB_FFBReport_SetEffect_Output_Data_t *)report)->directionX,
						((USB_FFBReport_SetEffect_Output_Data_t *)report)->directionY);
				break;
			case RID_O_SET_ENVELOPE:
				report = malloc(sizeof(USB_FFBReport_SetEnvelope_Output_Data_t));
				fread(report, sizeof(USB_FFBReport_SetEnvelope_Output_Data_t), 1, hid_dev);
				printf("Set Envelope Output Report read, sizeof %u\n",
						sizeof(USB_FFBReport_SetEnvelope_Output_Data_t));
				printf("blidx %u, attacklvl %u, fadelvl %u, attacktime %u, fadetime %u\n",
						((USB_FFBReport_SetEnvelope_Output_Data_t *)report)->effectBlockIndex,
						((USB_FFBReport_SetEnvelope_Output_Data_t *)report)->attackLevel,
						((USB_FFBReport_SetEnvelope_Output_Data_t *)report)->fadeLevel,
						((USB_FFBReport_SetEnvelope_Output_Data_t *)report)->attackTime,
						((USB_FFBReport_SetEnvelope_Output_Data_t *)report)->fadeTime);
				break;
			case RID_O_SET_CONDITION:
				report = malloc(sizeof(USB_FFBReport_SetCondition_Output_Data_t));
				fread(report, sizeof(USB_FFBReport_SetCondition_Output_Data_t), 1, hid_dev);
				printf("Set Condition Output Report read, sizeof %u\n",
						sizeof(USB_FFBReport_SetCondition_Output_Data_t));
				printf("blidx %u, pBlOffset %u, cpOffset %u, posCoef %u, negCoef %u, posSat %u, negSat %u, deadband %u\n",
						((USB_FFBReport_SetCondition_Output_Data_t *)report)->effectBlockIndex,
						((USB_FFBReport_SetCondition_Output_Data_t *)report)->parameterBlockOffset,
						((USB_FFBReport_SetCondition_Output_Data_t *)report)->cpOffset,
						((USB_FFBReport_SetCondition_Output_Data_t *)report)->positiveCoefficient,
						((USB_FFBReport_SetCondition_Output_Data_t *)report)->negativeCoefficient,
						((USB_FFBReport_SetCondition_Output_Data_t *)report)->positiveSaturation,
						((USB_FFBReport_SetCondition_Output_Data_t *)report)->negativeSaturation,
						((USB_FFBReport_SetCondition_Output_Data_t *)report)->deadBand);
				break;
			case RID_O_SET_PERIODIC:	
				report = malloc(sizeof(USB_FFBReport_SetPeriodic_Output_Data_t));
				fread(report, sizeof(USB_FFBReport_SetPeriodic_Output_Data_t), 1, hid_dev);
				printf("Set Periodic Output Report read, sizeof %u\n",
						sizeof(USB_FFBReport_SetPeriodic_Output_Data_t));
				printf("blidx %u, magnitude %u, offset %d, phase %u, period %u\n",
						((USB_FFBReport_SetPeriodic_Output_Data_t *)report)->effectBlockIndex,
						((USB_FFBReport_SetPeriodic_Output_Data_t *)report)->magnitude,
						((USB_FFBReport_SetPeriodic_Output_Data_t *)report)->offset,
						((USB_FFBReport_SetPeriodic_Output_Data_t *)report)->phase,
						((USB_FFBReport_SetPeriodic_Output_Data_t *)report)->period);
				break;
			case RID_O_SET_CONST_FORCE:
				report = malloc(sizeof(USB_FFBReport_SetConstantForce_Output_Data_t));
				fread(report, sizeof(USB_FFBReport_SetConstantForce_Output_Data_t), 1, hid_dev);
				printf("Set Constant Force Output Report read, sizeof %u\n", sizeof(USB_FFBReport_SetConstantForce_Output_Data_t));
				printf("blidx %u, magnitude %u\n",
						((USB_FFBReport_SetConstantForce_Output_Data_t *)report)->effectBlockIndex,
						((USB_FFBReport_SetConstantForce_Output_Data_t *)report)->magnitude);
				break;
			case RID_O_SET_RAMP_FORCE:
				report = malloc(sizeof(USB_FFBReport_SetRampForce_Output_Data_t));
				fread(report, sizeof(USB_FFBReport_SetRampForce_Output_Data_t), 1, hid_dev);
				printf("Set Ramp force Output Report read, sizeof %u\n", sizeof(USB_FFBReport_SetRampForce_Output_Data_t));
				printf("blidx %u, start %u, end %u\n",
						((USB_FFBReport_SetRampForce_Output_Data_t *)report)->effectBlockIndex,
						((USB_FFBReport_SetRampForce_Output_Data_t *)report)->start,
						((USB_FFBReport_SetRampForce_Output_Data_t *)report)->end);
				break;
			case RID_O_SET_EFFECT_OP:		
				report = malloc(sizeof(USB_FFBReport_EffectOperation_Output_Data_t));
				fread(report, sizeof(USB_FFBReport_EffectOperation_Output_Data_t), 1, hid_dev);
				printf("Set Effect Operation Output Report read, sizeof %u\n", sizeof(USB_FFBReport_EffectOperation_Output_Data_t));
				printf("blidx %u, operation %u, loopcount %u\n",
						((USB_FFBReport_EffectOperation_Output_Data_t *)report)->effectBlockIndex,
						((USB_FFBReport_EffectOperation_Output_Data_t *)report)->operation,
						((USB_FFBReport_EffectOperation_Output_Data_t *)report)->loopCount);
				break;
			case RID_O_SET_BLOCK_FREE:
				report = malloc(sizeof(USB_FFBReport_BlockFree_Output_Data_t));
				fread(report, sizeof(USB_FFBReport_BlockFree_Output_Data_t), 1, hid_dev);
				printf("Set Block Free Output Report read, sizeof %u\n", sizeof(USB_FFBReport_BlockFree_Output_Data_t));
				printf("blidx %u\n",
						((USB_FFBReport_BlockFree_Output_Data_t *)report)->effectBlockIndex);
				break;
			case RID_O_SET_DEVICE_OP:
				report = malloc(sizeof(USB_FFBReport_DeviceControl_Output_Data_t));
				fread(report, sizeof(USB_FFBReport_DeviceControl_Output_Data_t), 1, hid_dev);
				printf("Set Device Operation Output Report read, sizeof %u\n", sizeof(USB_FFBReport_DeviceControl_Output_Data_t));
				printf("control %u\n",
						((USB_FFBReport_DeviceControl_Output_Data_t *)report)->control);
				break;
			default:
				break;
		}
		if(report)
		{
			free(report);
			report = NULL;
		}
		recv_buf = 0;

	}
	
        // 200 Hz should be enough
        usleep(5000);
    }
    return 0;
}

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/fs.h>
#include <linux/string.h>

#define A 17
#define B 18

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Philippe Kalaf");
MODULE_DESCRIPTION("Decode quadrature signals from a rotary encoder");
MODULE_VERSION("0.01");

static unsigned int irq_number_a, irq_number_b;

static volatile long enc_count = 0;
static int cur_a = 1, cur_b = 0;  

static struct kobject *enc_count_kobject;
static ssize_t enc_count_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf);
static struct kobj_attribute enc_count_attribute =__ATTR(enc_count, S_IRUGO, enc_count_show,
                                                   NULL);
static irq_handler_t input_a_handler(unsigned int irq, void *dev_id, struct pt_regs *regs);
static irq_handler_t input_b_handler(unsigned int irq, void *dev_id, struct pt_regs *regs);

static int __init quad_dec_init(void) {
  printk(KERN_INFO "QUAD_DEC: Loading quadrature decoder driver\n");

  // Setup both input pins
  int err;
  if((err = gpio_request(A, "QUAD_A")) < 0)
  {
    printk(KERN_INFO "QUAD_DEC: Unable to request GPIO A errno%d\n", err);
    return -1;
  }
  else
    gpio_direction_input(A);

  if(gpio_request(B, "QUAD_B") < 0)
  {
    printk(KERN_INFO "QUAD_DEC: Unable to request GPIO B\n");
    return -1;
  }
  else
    gpio_direction_input(B);

  // Setup IRQ handler
  irq_number_a = gpio_to_irq(A);
  irq_number_b = gpio_to_irq(B);
  printk(KERN_INFO "QUAD_DEC: GPIO A has IRQ %d and GPIO B had IRQ %d\n", irq_number_a, irq_number_b);
  if(request_irq(irq_number_a, 
                 (irq_handler_t) input_a_handler,
		 IRQF_TRIGGER_RISING | IRQF_TRIGGER_FALLING, 
                 "quad_input_handler_a",
                 NULL) 
     < 0)
  {
    printk(KERN_INFO "QUAD_DEC: Could not request IRQ on GPIO A\n");
    return -1;
  }
  if(request_irq(irq_number_b, 
                 (irq_handler_t) input_b_handler,
		 IRQF_TRIGGER_RISING | IRQF_TRIGGER_FALLING, 
                 "quad_input_handler_b",
                 NULL)
    < 0)
  {
    printk(KERN_INFO "QUAD_DEC: Could not request IRQ on GPIO B\n");
    return -1;
  }

  enc_count_kobject = kobject_create_and_add("quad_dec_count", kernel_kobj);
  if(!enc_count_kobject)
  {
    printk(KERN_INFO "QUAD_DEC: Could not create kobject\n");
    return -1;
  }

  if(sysfs_create_file(enc_count_kobject, &enc_count_attribute.attr) < 0)
  {
    printk(KERN_INFO "QUAD_DEC: Could not create sysfs file\n");
    return -1;
  }

  return 0;
}

static void __exit quad_dec_exit(void) {
  printk(KERN_INFO "QUAD_DEC: Stopping and unloading quadrature decoder driver\n");
  free_irq(irq_number_a, NULL);
  free_irq(irq_number_b, NULL);
  gpio_free(A);
  gpio_free(B);
  kobject_put(enc_count_kobject);
  printk(KERN_INFO "QUAD_DEC: Done unloading quadrature decoder driver\n");
}

void decode_quad(void)
{
  static int8_t lookup_table[] = {0,-1,1,0,1,0,0,-1,-1,0,0,1,0,1,-1,0};
  static uint8_t enc_val = 0;

  // move up the old values and insert the new ones
  enc_val = enc_val << 2;
  enc_val = enc_val | (cur_a << 1);
  enc_val = enc_val | cur_b;

  enc_count = enc_count + lookup_table[enc_val & 0b1111];
 // printk(KERN_INFO "QUAD_DEC: enc_count is %ld\n", enc_count);

  return;
}

static irq_handler_t input_a_handler(unsigned int irq, void *dev_id, struct pt_regs *regs)
{
  cur_a = gpio_get_value(A);
  decode_quad();
  return (irq_handler_t) IRQ_HANDLED;
}

static irq_handler_t input_b_handler(unsigned int irq, void *dev_id, struct pt_regs *regs)
{
  cur_b = gpio_get_value(B);
  decode_quad();
  return (irq_handler_t) IRQ_HANDLED;
}

static ssize_t enc_count_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
  return sprintf (buf, "%ld\n", enc_count);
}

module_init(quad_dec_init);
module_exit(quad_dec_exit);

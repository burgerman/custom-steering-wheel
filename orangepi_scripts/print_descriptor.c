#include <inttypes.h>
#include <stdio.h>
#include <unistd.h>
//#include "HIDReportData.h"
//#include "FFBDescriptor.h"
// 
#include "burger_descriptor.h"
//#include "burger_descriptor_test.h"
//#include "other_descriptor.h"
//#include "keyb_desc.h"

// Works, taken from real device
//#include "steering_descriptor.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>

int main() {
/*
    for (int i=0;i<sz;i++) {
	printf("\\\\x%02hhx", pidReportDescriptor[i]);
    }
    printf("\n");
*/
  unlink("desc");
  int sdfd = open("desc",O_WRONLY | O_CREAT, 0644);
  if (sdfd == -1) {
    printf("bad open\n");
    exit(-1);
  }
  write(sdfd, pidReportDescriptor, pidReportDescriptorSize);
  close(sdfd);
}

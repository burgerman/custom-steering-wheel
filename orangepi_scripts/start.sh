#!/bin/sh
modprobe libcomposite
ORIG=$(pwd)

cd /sys/kernel/config/usb_gadget/
mkdir -p burgerwheel
cd burgerwheel
echo "64" > bMaxPacketSize0
echo "0x200" > bcdUSB
echo "0x100" > bcdDevice
echo 0x045e > idVendor
echo 0x001b > idProduct
echo 0x00 > bDeviceClass
echo 0x00 > bDeviceSubClass
echo 0x00 > bDeviceProtocol
#bmAttributes
mkdir -p strings/0x409
echo "0000000001" > strings/0x409/serialnumber
echo "burgerfarm" > strings/0x409/manufacturer
echo "HID Gadget" > strings/0x409/product

mkdir -p configs/c.1/strings/0x409
echo "Conf 1" > configs/c.1/strings/0x409/configuration
echo 120 > configs/c.1/MaxPower
#echo 0xC0 > configs/c.1/bmAttributes

mkdir -p functions/hid.usb0
echo 1 > functions/hid.usb0/protocol
echo 1 > functions/hid.usb0/subclass
echo 64 > functions/hid.usb0/report_length
cp ${ORIG}/desc functions/hid.usb0/report_desc
ln -s functions/hid.usb0 configs/c.1/

ls /sys/class/udc > UDC

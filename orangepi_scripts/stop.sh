#!/bin/sh
modprobe libcomposite
ORIG=$(pwd)

cd /sys/kernel/config/usb_gadget/
echo "" >burgerwheel/UDC
rm burgerwheel/configs/c.1/hid.usb0
rmdir burgerwheel/configs/c.1/strings/0x409/
rmdir burgerwheel/configs/c.1
rmdir burgerwheel/functions/hid.usb0/
rmdir burgerwheel/strings/0x409/
rmdir burgerwheel
